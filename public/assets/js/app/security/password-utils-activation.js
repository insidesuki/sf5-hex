const passwordActivation = document.getElementById('change_password_form_plainPassword_first');
const passwordConfirmation = document.getElementById('change_password_form_plainPassword_second');
const result = document.getElementById('result');
const showPassword = document.getElementById('show-password');


showPassword.addEventListener('click', () => {
    if (passwordActivation.type === 'password') {
        passwordActivation.type = 'text';
        passwordConfirmation.type = 'text';
        showPassword.innerHTML = '<li class="bx bx-show-alt"></i> Hide';
    } else {
        passwordActivation.type = 'password';
        passwordConfirmation.type = 'password';
        showPassword.innerHTML = '<li class="bx bx-show"></i> Show';
    }
});

const checkStrengthPassword = (event) => {

    const password = document.querySelector('#change_password_form_plainPassword_first').value;
    const passwordVerification = document.querySelector('#change_password_form_plainPassword_second').value;
    const buttonActivate = document.getElementById("btn-activar");
    let strength = 0;
    if (password.length < 12) {
        result.classList.remove('bg-info');
        result.classList.remove('bg-success');
        result.classList.remove('bg-warning');
        result.classList.add('progress-bar');
        result.classList.add('bg-danger');
        result.style.width = '10%';
        result.textContent = 'Very short';
        buttonActivate.disabled = true;
    }

    if (password.length > 11) {
        strength += 1;
    }
    // If password contains both lower and uppercase characters, increase strength value.
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
        strength += 1;
    }
    // if password contains numbers and numbers
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
        strength += 1;
    }



    if (strength < 2) {
        result.classList.remove('bg-danger');
        result.classList.remove('bg-info');
        result.classList.remove('bg-success');
        result.classList.add('progress-bar');
        result.classList.add('bg-warning');
        result.style.width = '30%';
        buttonActivate.disabled = true;
        result.textContent = strength + '.Weak';
    }

    else if(strength === 2){
        result.classList.remove('bg-danger');
        result.classList.remove('bg-warning');
        result.classList.remove('bg-success');
        result.classList.add('progress-bar');
        result.classList.add('bg-info');
        result.style.width = '60%';
        result.textContent = strength + '.Almost...';
        buttonActivate.disabled = true;

    }
    else {
        result.classList.remove('bg-info');
        result.classList.remove('bg-danger');
        result.classList.remove('bg-warning');
        result.classList.add('progress-bar');


        if (password !== passwordVerification) {
            result.classList.remove('bg-success');
            result.style.width = '90%';
            result.classList.add('bg-primary');
            result.textContent = strength + '.Please confirm password ';
            buttonActivate.disabled = true;
        } else {

            result.style.width = '100%';
            result.classList.add('bg-success');
            result.textContent = strength + '.All ok!!!';
            buttonActivate.disabled = false;

        }


    }


}

passwordActivation.addEventListener("keyup", checkStrengthPassword);
passwordConfirmation.addEventListener("keyup", checkStrengthPassword);

