const password = document.getElementById('password');
const showPassword = document.getElementById('show-password');
const capsLockMessage = document.getElementById('caps-lock');
// show password
showPassword.addEventListener('click', () => {
    if (password.type === 'password') {

        password.type = 'text';
        showPassword.innerHTML = '<li class="lni lni-unlock"></i>';
    } else {
        password.type = 'password';
        showPassword.innerHTML = '<li class="lni lni-lock"></i>';
    }
});

// capslock detection
const evaluateCapsLock = (event) => {
    let eval;
    if (event.getModifierState('Shift') === true) {
        eval = true;

    } else eval = event.getModifierState('CapsLock') === true;

    if (eval) {
        capsLockMessage.innerHTML = '<span class="text-info">BloqMayus on</span>';
    } else {
        capsLockMessage.innerHTML = '<span class="text-primary small">BloqMayus off</span>';
    }

};
//
document.addEventListener("keydown", evaluateCapsLock);
document.addEventListener("keyup", evaluateCapsLock); // Agregado para detectar la liberación de la tecla
