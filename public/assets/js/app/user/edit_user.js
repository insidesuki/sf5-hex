import {FormUtility} from "../../FormUtility/FormUtility.js";

const editForm = new FormUtility('formEditMyData');

document.getElementById('editMyData').addEventListener('click', function() {

    editForm.enable();

});

document.getElementById('cancelEdit').addEventListener('click', function() {

    editForm.disable();

});