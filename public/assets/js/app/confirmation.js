document.addEventListener('DOMContentLoaded', function () {
    const modal = new bootstrap.Modal(document.getElementById('operationConfirmationModal'));
    const buttons = document.querySelectorAll('.actionButton');

    if (buttons.length > 0) {
        buttons.forEach(function (button) {
            button.addEventListener('click', function () {
                const content = this.getAttribute('data-content');
                const url = this.getAttribute('data-url-operation');
                const headerClass = this.getAttribute('data-header-class');
                const errorDiv = document.getElementById('confirmation-error');
                const confirmationHeader = document.getElementById('confirmation-header');

                if (confirmationHeader && headerClass) {
                    confirmationHeader.classList.add(headerClass);
                }
                errorDiv.innerHTML = '';

                document.querySelector('.confirmation-body').innerHTML = `${content}`;

                modal.show(); // Mostrar el modal directamente después de configurar su contenido
                setConfirmOperationEvent(url); // Configurar el evento click fuera del bucle

                // Limpiar configuraciones específicas cuando se oculta el modal
                modal._element.addEventListener('hidden.bs.modal', function () {
                    confirmationHeader.classList.remove(headerClass);
                    errorDiv.innerHTML = '';
                });
            });
        });
    }

    function setConfirmOperationEvent(url) {
        document.getElementById('confirmOperation').addEventListener('click', function () {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function () {
                if (xhr.status === 200) {
                    window.location.reload();
                } else {
                    showErrorModal(`Error: ${xhr.status} - ${xhr.responseText}`);
                }
            };

            xhr.send();
        });
    }

    function showErrorModal(errorMessage) {
        const errorDiv = document.getElementById('confirmation-error');

        if (errorDiv) {
            errorDiv.textContent = errorMessage;
            errorDiv.classList.add('alert', 'alert-danger');
            errorDiv.innerHTML += '<span class="bi bi-exclamation-triangle-fill"></span>';

            errorDiv.style.display = 'block';

            setTimeout(function () {
                errorDiv.style.display = 'none';
                modal.hide();
            }, 5000);
        } else {
            console.log('Error Div Not Found');
        }
    }
});
