/**
 * ClassPrinter class.
 * Collect all html elements referenced by $classReference, create html content and show print dialog
 * @version 1.0
 * @date 2023
 * @author avadillo@iprprevencion.es, jgarciat@iprprevencion.es, sys@iprprevencion.es
 * @example:
 *  const printer = new ClassPrinter('yourClassName');
 *  printer.printByClassName();
 */

import {JsPrintDialog} from "./JsPrintDialog.js";
import {HtmlTemplate} from "./HtmlTemplate.js";

export class ClassPrinter extends JsPrintDialog {


    #cssTyle;

    constructor(classReference, logoId = 'none') {
        super();
        this.classReference = classReference;
        this.logoId = logoId;
        this.#cssTyle = ` 
                  body {
                    font-family: Arial, sans-serif;
                  }
            
                  .${classReference} {
                    padding: 5px;
                    margin: 10px;
                    font-size: 16px;
                  }
            
                  .logo {
                    width: 200px;
                    height: auto;
                  }
        
        
        `;
    }

    /**
     * Public method: Print html content by class name
     */
    handle(cssStyle = null) {
        try {

            if (null !== cssStyle) {
                this.#cssTyle = cssStyle;
            }

            const htmlContent = this.#generateHTMLContentByClassName();
            this.showPrintDialog(htmlContent);
        } catch (e) {
            console.error(e);
        }

    }

    /**
     * Private method: Generates the HTML content for the printable elements.
     * @returns {string} - The generated HTML content
     */
    #generateHTMLContentByClassName() {
        const printableElements = Array.from(document.getElementsByClassName(this.classReference));

        if (!printableElements || printableElements.length === 0) {
            throw new Error('Not exists any printable elements with class:' + this.classReference);
        }
        const logotype = document.getElementById(this.logoId);

        const logoDiv = logotype
            ? `<div><img src="${logotype.src}" class="logo" alt="Logo"></div><hr>`
            : '';

        const content = printableElements.map((element) => element.outerHTML).join('<hr>');
        const htmlParser = new HtmlTemplate('title1', this.#cssTyle);

        return htmlParser.parse(logoDiv + content);
    }


}

