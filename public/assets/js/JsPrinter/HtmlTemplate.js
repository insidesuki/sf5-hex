/**
 * HtmlTemplate class
 * Parse content into a basic html structure
 * @version 0.0.1
 * @author sys@iprprevencion.es
 */
export class HtmlTemplate {

    #baseTemplate = '';
    #title;
    #cssTyle = `
    
     body {
            font-family: Arial, sans-serif;
            font-size: 11px;
            
          }
    
    `;

    constructor(title, cssStyle = null) {

        this.#title = title;
        if (null !== cssStyle) {
            this.#cssTyle = cssStyle;
        }

    }

    parse(content) {

        return `
            <html lang="es">
              <head>
                <style>
                ${this.#cssTyle}
                </style>
                <title>${this.#title}</title>
              </head>
              <body>
                ${content}
              </body>
            </html>
        `;


    }


}