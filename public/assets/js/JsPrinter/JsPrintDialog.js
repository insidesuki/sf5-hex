/**
 * JsPrintDialog class
 * show a html content and display browser print dialog
 * @version 0.0.2
 * @author sys@iprprevencion.es
 */


export class JsPrintDialog {


    /**
     * Public method: Writes the HTML content to the iframe's document and prints it.
     * @param {string} content - The HTML content to be printed
     */

    showPrintDialog(content) {
        const printFrame = document.createElement('iframe');
        printFrame.style.display = 'none';
        document.body.appendChild(printFrame);

        const printDocument = printFrame.contentWindow.document;

        printDocument.open('', '_blank');
        printDocument.write(content);
        printDocument.close();

        // explicit request print permission
        printFrame.contentWindow.print();

        // delay one second before removing the iframe (chrome issue)
        setTimeout(() => {
            document.body.removeChild(printFrame);
        }, 1000);
    }


}