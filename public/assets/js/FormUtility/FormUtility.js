export class FormUtility {

    #form;
    #actionsDiv;
    #originalValues;

    constructor(formId, actionsDiv = 'formActions') {
        try {
            this.#create(formId, actionsDiv);
            this.#originalValues = {};
            this.#saveOriginalValues();
        } catch (e) {
            console.error(e);
        }
    }

    #create(formId, actionsDiv) {
        this.#form = document.getElementById(formId);
        this.#actionsDiv = document.getElementById(actionsDiv);

        if (!this.#form) {
            throw new Error(`Form with id: ${formId} not found!!`);
        }

        if (!this.#actionsDiv) {
            throw new Error(`Actions forms with Div with id: ${actionsDiv} not found!!`);
        }
    }

    get elements() {
        return this.#form.elements;
    }

    #isModifiableElement(element) {
        return (element.tagName === 'INPUT' || element.tagName === 'TEXTAREA' || element.tagName === 'SELECT') &&
            element.type !== 'email';
    }

    #saveOriginalValues() {
        const elements = this.elements;

        for (const element of elements) {
            if (this.#isModifiableElement(element)) {
                const id = element.name;
                this.#originalValues[id] = { value: element.value, id };
            }
        }
    }

    enable() {
        this.#updateFormElements(false);
        this.#actionsDiv.hidden = false;
    }

    disable() {
        this.#restoreOriginalValues();
        this.#updateFormElements(true);
        this.#actionsDiv.hidden = true;
    }

    #updateFormElements(status) {
        const elements = this.elements;

        for (const element of elements) {

            element.disabled = status;
            element.style.display = 'inline';
        }
    }

    #restoreOriginalValues() {
        const elements = this.elements;

        for (const element of elements) {
            if (this.#isModifiableElement(element)) {
                const id = element.name;
                this.#originalValues[id] = { ...this.#originalValues[id], value: element.value };
            }
        }
    }

    submit() {
        this.#form.submit();
    }
}
