<?php

declare(strict_types=1);

namespace Shared\Infrastructure\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractRequestCommand extends DtoSerializer
{

    public static function createFromRequest(Request $request): self
    {
        return parent::createFromArray($request->request->all());
    }

}
