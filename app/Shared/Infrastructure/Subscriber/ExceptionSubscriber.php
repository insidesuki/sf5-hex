<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Subscriber;

use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

final class ExceptionSubscriber extends AbstractCommonAction implements EventSubscriberInterface
{

	public static function getSubscribedEvents(): array
	{
		return [
			KernelEvents::EXCEPTION => 'onKernelException'
		];
	}

	public function onKernelException(ExceptionEvent $exceptionEvent): void
	{

		$throwable = $exceptionEvent->getThrowable();

		if($throwable instanceof NotFoundHttpException){

			$exceptionEvent->setResponse($this->render('Messages/error_404.html.twig',[
				'message' => $throwable->getMessage()
			]));
		}


	}
}