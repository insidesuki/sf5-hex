<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Web;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractRequestCommand extends DtoSerializer
{


	public static function fromRequest(Request $request): static
	{
		$formData = $request->request->all();
		return parent::createFromArray($formData);
	}



}