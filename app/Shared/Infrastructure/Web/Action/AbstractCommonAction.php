<?php

namespace Shared\Infrastructure\Web\Action;

use Insidesuki\Utilities\File\File;
use InvalidArgumentException;
use RuntimeException;
use Shared\Application\Service\Report\Contract\ReportGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;
use function count;

abstract class AbstractCommonAction extends AbstractController
{


    public $defaultSession;


    /**
     * @throws \JsonException
     */
    protected function getJsonDataAsArray(Request $request): array
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (count($data) === 0) {
            throw new InvalidArgumentException('Empty JsonBody');
        }

        return $data;
    }

    protected function redirectToReferer(Request $request): Response
    {

        return $this->redirect($request->headers->get('referer'));

    }

    /**
     * Validate a dto using Validator
     * @TODO , for some estrange reason, the violations collections repeats, not uniques
     * violations
     */
    protected function validateDto(object $dto,ValidatorInterface $validator): void
    {
        // validate dto
        $constraintViolationList = $validator->validate($dto);

        if (count($constraintViolationList) > 0) {
            $violationsString = (string)$constraintViolationList;
            throw new InvalidArgumentException($violationsString);
        }
    }

    protected function responseFileToDownload(
        string  $fileName,
        string  $disposition = 'attachment',
        bool    $deleteFile = true,
        ?string $path = null
    ): Response
    {

        $pathDirectory = (null === $path) ? $this->getParameter('upload_dir') : realpath($path);
        $pathWithFile = $pathDirectory . '/' . $fileName;
        $fileinfoMimeTypeGuesser = new FileinfoMimeTypeGuesser();
        $binaryFileResponse = new BinaryFileResponse($pathWithFile);
        // Set the mimetype with the guesser or manually
        if ($fileinfoMimeTypeGuesser->isGuesserSupported()) {
            // Guess the mimetype of the file according to the extension of the file
            $binaryFileResponse->headers->set('Content-Type', $fileinfoMimeTypeGuesser->guessMimeType($pathWithFile));
        } else {
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $binaryFileResponse->headers->set('Content-Type', 'text/plain');
        }

        // Set content disposition inline of the file
        $binaryFileResponse->setContentDisposition(
            $disposition,
            $fileName
        );
        $binaryFileResponse->deleteFileAfterSend($deleteFile);
        return $binaryFileResponse;


    }

    protected function streamPdf(ReportGeneratorInterface $reportGenerator, array $disposition = ['Atttachment' => false]): Response
    {
        return new Response(
            $reportGenerator->stream($disposition),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/pdf'
            ]

        );

    }

    protected function getUserSession(): UserInterface
    {

        $user = $this->getUser();
        if (null === $user) {
            throw new RuntimeException('UserSession not available!!!');
        }

        return $user;

    }

    protected function storeInSession(Request $request, string $key, mixed $value): void
    {

        $this->setSession($request);
        $this->defaultSession->set($key,$value);
    }

    protected function getFromSession(Request $request, string $key): mixed
    {

        $this->setSession($request);
        return $this->defaultSession->get($key);
    }

    protected function getFromSessionOrFail(Request $request, string $key)
    {

        $value = @$this->getFromSession($request, $key);
        if (null === $value) {
            throw new RuntimeException(sprintf('Value:%s, does not exists in Session', $key));
        }

        return $value;

    }


    private function setSession(Request $request):void{
        if(null === $this->defaultSession){
            $this->defaultSession = $request->getSession();
        }
    }


    protected function showSuccessfullOperation(string $message = ''):Response{


        $user = $this->getUserSession();
        return $this->render('Gui/Twig/Messages/success_operation.html.twig',[
            'message' => $message,
            'baseHtml' => $user->profile()->baseTemplate
        ]);

    }


    public function showErrorPage(Throwable $throwable): Response
    {

        return $this->render('Gui/Twig/Messages/error_operation.html.twig', [
            'exception' => $throwable,
        ],new Response('', 400));

    }



    public function show404(string $message = ''): Response
    {

        return $this->render('Gui/Twig/Messages/error_404.html.twig', [
            'message' => $message,
        ], new Response('', 404));

    }


    protected function downloadFileResponse(File $file, bool $deleteFile = true): Response
    {

        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        $response = new BinaryFileResponse($file->fullPath);
        if ($mimeTypeGuesser->isGuesserSupported()) {
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($file->fullPath));
        } else {
            $response->headers->set('Content-Type', 'text/plain');
        }
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file->baseName
        );
        $response->deleteFileAfterSend($deleteFile);
        return $response;

    }


    protected function executeAction(callable $callable): Response
    {
        try {
            return $callable();
        } catch (Throwable $throwable) {
            return $this->showErrorPage($throwable);
        }
    }




    protected function streamPdfFromContent(string $pdfContent, string $fileName): Response
    {
        return new Response(
            $pdfContent,
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '"'
            ]
        );


    }





}