<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Web\User\Detail;


use Admin\Infrastructure\Web\User\Presentation\UserArrayPresenter;
use Security\Application\User\DetailUser\RetrieveUser;
use Security\Domain\Log\Repository\LogRepository;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Shared\Infrastructure\Web\User\ModifyUser\ModifyUserAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;


class DetailUserAction extends AbstractCommonAction
{

	public function __construct(
		private readonly RetrieveUser $retrieveUser,
		private readonly ModifyUserAction $modifyUserAction,
		private readonly LogRepository $logRepository
	)
	{
	}

	public function __invoke(Request $request): Response
	{

		try {

			$user = $this->getUserSession();
			$idUser = $user->idUser();

			if ($request->isMethod('POST')) {

				return ($this->modifyUserAction)($idUser,$request);

			}

			$userPresentation = $this->retrieveUser->handle(
				idUser: $idUser,
				userPresenter: new UserArrayPresenter()
			);

			return $this->render('User/Detail/user_detail.html.twig', [
				'user' => $userPresentation->read(),
				'lastLogs' => $this->logRepository->findAllByIdUser($idUser)
			]);

		} catch (Throwable $throwable) {
			return $this->showErrorPage($throwable);
		}


	}

}