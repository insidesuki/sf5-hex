<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Web\User\ModifyUser;

use Security\Application\User\ChangeUserData;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ModifyUserAction extends AbstractCommonAction
{


	public function __construct(
		private readonly ChangeUserData $changeUserData
	)
	{
	}

	public function __invoke(string $idUser, Request $request): Response
	{

		try {

			$this->changeUserData->handle(
				idUser: $idUser,
				changeUserDataCommand: ChangeUserFromRequestDataCommand::fromRequest($request)
			);

			return $this->redirectToReferer($request);

		} catch (Throwable $throwable) {

			return $this->showErrorPage($throwable);

		}


	}

}