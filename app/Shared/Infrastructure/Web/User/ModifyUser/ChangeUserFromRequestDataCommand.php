<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Web\User\ModifyUser;

use Security\Domain\User\Command\ChangeUserDataCommandInterface;
use Shared\Infrastructure\Web\AbstractRequestCommand;

class ChangeUserFromRequestDataCommand extends AbstractRequestCommand implements ChangeUserDataCommandInterface
{

	private string $name;
	private string $email;

	public function __construct(string $name, string $email)
	{
		$this->name = $name;
		$this->email = $email;
	}


	public function name(): string
	{
		return $this->name;
	}

	public function email(): string
	{
		return $this->email;
	}


}