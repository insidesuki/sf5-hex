<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\File\Exception;

use RuntimeException;

final class InvalidFileTypeException extends RuntimeException
{
	public function __construct(string $type)
	{
		parent::__construct(sprintf('Invalid FileType, must be a "%s" file.', $type));
	}
}