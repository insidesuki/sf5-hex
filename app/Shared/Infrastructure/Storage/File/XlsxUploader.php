<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\File;

use Shared\Infrastructure\Storage\File\AbstractFileUploader;
use Shared\Infrastructure\Storage\File\Exception\FolderDoesNotExistsException;
use Shared\Infrastructure\Storage\File\Exception\InvalidFileTypeException;

final class XlsxUploader extends AbstractFileUploader
{

	public function __invoke(string $directory): string
	{
		if ($this->uploadedFile->getClientMimeType() !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
			throw new InvalidFileTypeException('.xlsx');
		}

		if (!is_dir($directory)) {
			throw new FolderDoesNotExistsException();
		}

		// rename
		$filename = md5(uniqid('', true)) . '.xlsx';

		// upload
		$this->uploadedFile->move($directory, $filename);

		return $directory .'/'.$filename;
	}

}