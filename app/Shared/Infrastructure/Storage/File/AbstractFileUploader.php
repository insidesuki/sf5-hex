<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractFileUploader
{
	protected \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile;

	public function __construct(UploadedFile $uploadedFile)
	{

		$this->uploadedFile = $uploadedFile;

	}

}