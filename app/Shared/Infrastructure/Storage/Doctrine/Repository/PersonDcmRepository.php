<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Storage\Doctrine\Repository;

use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Shared\Domain\Model\Person;
use Shared\Domain\Repository\PersonRepository;

class PersonDcmRepository extends AbstractDoctrineRepository implements PersonRepository
{

	/**
	 * @inheritDoc
	 */
	protected static function entityClass(): string
	{
		return Person::class;
	}

	public function store(Person $person): Person
	{
		$this->saveEntity($person);
		return $person;
	}
}