<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\Doctrine\Repository;

use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Shared\Domain\Model\EventStore;
use Shared\Domain\Repository\EventStoreRepository;

final class EventStoreDcmRepository extends AbstractDoctrineRepository implements EventStoreRepository
{

	/**
	 * @inheritDoc
	 */
	protected static function entityClass(): string
	{
		return EventStore::class;
	}

	public function store(EventStore $eventStore): void
	{
		$this->saveEntity($eventStore);
	}
}