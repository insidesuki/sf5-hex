<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Api;

use InvalidArgumentException;
use Shared\Common\Domain\Exception\InvalidDataException;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiAction extends AbstractCommonAction
{


	protected function created(string $message): JsonResponse
	{
		$response = 'Resource created. #ID:' . $message;
		return $this->apiResponse($response, Response::HTTP_CREATED);
	}

	private function apiResponse(mixed $message, int $code = Response::HTTP_OK): JsonResponse
	{
		return new JsonResponse($message, $code);
	}

	protected function exception(string $message, int $code = Response::HTTP_BAD_REQUEST): JsonResponse
	{
		return $this->apiResponse($message, $code);
	}

	protected function updated(string $message = 'Updated'): JsonResponse
	{
		return $this->apiResponse($message, 200);
	}

	protected function notFound(string $message = 'Resource not found!'): JsonResponse
	{
		return $this->apiResponse($message, Response::HTTP_NOT_FOUND);
	}

	protected function responseData(array $data): JsonResponse
	{
		return $this->apiResponse($data, 200);
	}

    protected function executeApiAction(callable $callable): JsonResponse
    {
        try {
            return $callable();
        }
        catch (InvalidArgumentException | InvalidDataException $invalidArgumentException) {
            return new JsonResponse($invalidArgumentException->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        catch (Throwable  $throwable) {
            return new JsonResponse($throwable->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


}