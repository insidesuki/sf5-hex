<?php
declare(strict_types=1);

namespace Shared\Application\Service\Log;

use RuntimeException;

final class ParseLog
{


    public function handle(string $logFile): array
    {

        if (false === file_exists($logFile)) {
            throw new RuntimeException(sprintf('LogFile %s, does not exists.', $logFile));
        }

        $content = file_get_contents($logFile);
        $logLines = explode("\n", $content);

        $resultArray = [];

        foreach ($logLines as $logLine) {
            $matches = [];
            if (preg_match('/\[(.*?)\] ([^:]+): (.*)$/', $logLine, $matches)) {
                $timestamp = $matches[1];
                $logLevel = $matches[2];
                $logContent = $matches[3];


                $resultArray[] = [
                    'timestamp' => $timestamp,
                    'logLevel' => $logLevel,
                    'logContent' => $logContent
                ];
            }
        }

        return $resultArray;

    }

}