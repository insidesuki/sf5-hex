<?php

namespace Shared\Application\Service\Report\Contract;

interface ReportGeneratorInterface
{

    public function loadTemplate(string $template, array $parameters = []);

    public function loadTemplateForMultiple(array $parameters = []);

    public function setOptions(array $options): void;


    public function render();

    public function setName(string $pdfName): void;

    public function stream(array $options = []);

    public function save(string $pathToSave): void;

    public function toBase64(): string;

    public function name(): string;

}