<?php
declare(strict_types=1);

namespace Shared\Application\Service\ApiEvent;

use Shared\Domain\Command\ApiRequestCommandInterface;
use Shared\Domain\Model\ApiEvent;
use Shared\Domain\Repository\ApiEventRepository;

final class RegisterApiRequest
{
	public function __construct(
		private readonly ApiEventRepository $apiEventRepository
	)
	{
	}

	public function handle(ApiRequestCommandInterface $apiRequestCommand): ApiEvent
	{

		$apiEvent = ApiEvent::requestEvent($apiRequestCommand);
		$this->apiEventRepository->store($apiEvent);
		return $apiEvent;

	}

}