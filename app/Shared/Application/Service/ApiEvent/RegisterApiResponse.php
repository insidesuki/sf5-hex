<?php
declare(strict_types = 1);

namespace Shared\Application\Service\ApiEvent;
use Shared\Domain\Command\ApiResponseCommandInterface;
use Shared\Domain\Model\ApiEvent;
use Shared\Domain\Repository\ApiEventRepository;

final class RegisterApiResponse
{

	public function __construct(private readonly ApiEventRepository $apiEventRepository){}

	public function handle(string $idApiEvent, ApiResponseCommandInterface $apiResponseCommand): ApiEvent
	{

		$apiEvent = $this->apiEventRepository->findApiEvent($idApiEvent);
		$apiEvent->responseEvent($apiResponseCommand);
		return $this->apiEventRepository->store($apiEvent);

	}

}