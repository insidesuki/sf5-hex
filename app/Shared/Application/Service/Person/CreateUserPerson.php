<?php
declare(strict_types=1);

namespace Shared\Application\Service\Person;

use Security\Domain\User\Repository\UserRepository;
use Shared\Domain\Model\Person;
use Shared\Domain\Repository\PersonRepository;

class CreateUserPerson
{

	public function __construct(
		private readonly UserRepository   $userRepository,
		private readonly PersonRepository $personRepository
	)
	{
	}


	public function handle(string $idUser): Person
	{

		$user = $this->userRepository->findByIdUser($idUser);
		$person = Person::createFromUser($user);
		return $this->personRepository->store($person);

	}
}