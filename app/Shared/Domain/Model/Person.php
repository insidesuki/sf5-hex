<?php
declare(strict_types=1);

namespace Shared\Domain\Model;

use Security\Domain\User\Entity\User;
use Shared\Domain\Model\ValueObject\DocumentNumber;
use Shared\Domain\Model\ValueObject\Email;
use Shared\Domain\Model\ValueObject\Phone;
use Symfony\Component\Uid\Uuid;

class Person
{

	private string $idPerson;
	private string $fullName;
	private User $user;
	private DocumentNumber $identification;
	private Email $email;
	private Phone $phone;
	private ?string $otherInfo = null;
	private ?string $sign = null;
	private ?string $avatar = null;

	private function __construct(
		string         $fullName,
		User           $user,
		DocumentNumber $identification,
		Email          $email,
		Phone          $phone

	)
	{
		$this->idPerson = Uuid::v4()->toRfc4122();
		$this->fullName = $fullName;
		$this->user = $user;
		$this->identification = $identification;
		$this->email = $email;
		$this->phone = $phone;

		$this->user->assignPerson($this);
	}

	public static function createFromUser(User $user): self
	{

		return new self(
			fullName: $user->name(),
			user: $user,
			identification: DocumentNumber::create('none', false),
			email: Email::create($user->email()),
			phone: Phone::createAllowEmpty('')
		);

	}

	public function email(): Email
	{
		return $this->email;
	}

	public function idPerson(): string
	{
		return $this->idPerson;
	}

	public function fullName(): string
	{
		return $this->fullName;
	}

	public function user(): User
	{
		return $this->user;
	}

	public function identification(): DocumentNumber
	{
		return $this->identification;
	}

	public function otherInfo(): ?string
	{
		return $this->otherInfo;
	}

	public function sign(): ?string
	{
		return $this->sign;
	}

	public function phone(): Phone
	{
		return $this->phone;
	}

	public function avatar(): ?string
	{
		return $this->avatar;
	}


}