<?php
declare(strict_types=1);

namespace Shared\Domain\Model\ValueObject;

use Insidesuki\ValueObject\String\DocumentNumber\DocumentNumber AS DocumentNumberVo;

final class DocumentNumber extends DocumentNumberVo
{

}