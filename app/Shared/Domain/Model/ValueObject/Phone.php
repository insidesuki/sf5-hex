<?php
declare(strict_types=1);

namespace Shared\Domain\Model\ValueObject;

use Insidesuki\ValueObject\String\Phone\PhoneValue;

final class Phone extends PhoneValue
{

}