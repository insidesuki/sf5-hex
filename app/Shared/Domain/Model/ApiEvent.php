<?php
declare(strict_types = 1);

namespace Shared\Domain\Model;

use DateTime;
use Shared\Domain\Command\ApiRequestCommandInterface;
use Shared\Domain\Command\ApiResponseCommandInterface;
use Symfony\Component\Uid\Uuid;

class ApiEvent
{

	private const SUCCESS_RESPONSES = ['200', '201'];

	private readonly string    $idApiEvent;

	private readonly string    $appName;

	private readonly string    $url;

	private readonly string    $scope;

	private readonly string    $method;

	private string    $code;

	private string    $status = 'queued';

	private readonly DateTime  $startTime;

	private ?DateTime $finishTime;

	private readonly string    $payload;

	private string    $response;

	private readonly string    $username;

	private readonly string    $ipOrigin;


	private function __construct(
		string $appName,
		string $url,
		string $scope,
		string $method,
		string $payload,
		string $username,
		string $ip
	)
	{
		$this->idApiEvent = Uuid::v4()->toRfc4122();
		$this->appName    = $appName;
		$this->url        = $url;
		$this->scope      = $scope;
		$this->method     = $method;
		$this->payload    = $payload;
		$this->startTime  = new DateTime();
		$this->username   = $username;
		$this->ipOrigin   = $ip;

	}


	public static function requestEvent(ApiRequestCommandInterface $apiRequestCommand): self
	{

		return new self(
			$apiRequestCommand->appName(),
			$apiRequestCommand->url(),
			$apiRequestCommand->scope(),
			$apiRequestCommand->method(),
			$apiRequestCommand->payload(),
			$apiRequestCommand->username(),
			$apiRequestCommand->ipOrigin()
		);

	}

	public function appName(): string
	{
		return $this->appName;
	}

	public function url(): string
	{
		return $this->url;
	}

	public function scope(): string
	{
		return $this->scope;
	}

	public function method(): string
	{
		return $this->method;
	}

	public function payload(): string
	{
		return $this->payload;
	}

	public function username(): string
	{
		return $this->username;
	}

	public function ipOrigin(): string
	{
		return $this->ipOrigin;
	}

	public function responseEvent(ApiResponseCommandInterface $apiResponseCommand): void
	{

		$this->code       = $apiResponseCommand->code();
		$this->response   = $apiResponseCommand->response();
		$this->status     = (in_array($this->code, self::SUCCESS_RESPONSES)) ? 'success' : 'exception';
		$this->finishTime = new DateTime();

	}

	public function code(): string
	{
		return $this->code;
	}

	public function response(): string
	{
		return $this->response;
	}

	public function idApiEvent(): string
	{
		return $this->idApiEvent;
	}

	public function status(): string
	{
		return $this->status;
	}


	public function startTime(): DateTime
	{
		return $this->startTime;
	}

	public function finishTime(): ?DateTime
	{
		return $this->finishTime;
	}


}