<?php

namespace Shared\Domain\Model\Presenter;

interface CollectionPresenterInterface
{

	public function write(array $collection):self;

	public function read():mixed;

}