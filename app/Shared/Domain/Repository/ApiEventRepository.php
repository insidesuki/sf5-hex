<?php

namespace Shared\Domain\Repository;
use Shared\Domain\Model\ApiEvent;

interface ApiEventRepository
{
	public function findApiEvent(string $idLogEvent):ApiEvent;

	public function store(ApiEvent $apiEvent):ApiEvent;

}