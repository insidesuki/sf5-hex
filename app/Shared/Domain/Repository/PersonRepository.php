<?php

namespace Shared\Domain\Repository;

use Shared\Domain\Model\Person;

interface PersonRepository
{

	public function store(Person $person): Person;

}