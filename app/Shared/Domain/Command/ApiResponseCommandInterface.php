<?php

namespace Shared\Domain\Command;
interface ApiResponseCommandInterface
{

	public function code():string;

	public function response():string;

}