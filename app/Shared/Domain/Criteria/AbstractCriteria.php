<?php

declare(strict_types=1);

namespace Shared\Domain\Criteria;


abstract class AbstractCriteria
{


    public function __toArray(): array
    {
        $criteria = [];
        $mapping = $this->criteriaMapping();

        foreach ($mapping as $property => $key) {
            if ($this->$property) {
                $criteria[$key] = trim($this->$property);
            }
        }

        return $criteria;
    }

    abstract public function criteriaMapping(): array;

}