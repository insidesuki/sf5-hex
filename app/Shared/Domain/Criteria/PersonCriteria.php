<?php

declare(strict_types=1);

namespace Shared\Domain\Criteria;

final class PersonCriteria extends AbstractCriteria
{

    private function __construct(
        public readonly string $documentNumber = '',
        public readonly string $email = '',
        public readonly string $phone = ''
    ) {
    }

    public static function create(string $documentNumber = '', string $email = '', string $phone = ''): self
    {
        return new self(
            documentNumber: $documentNumber,
            email: $email,
            phone: $phone
        );
    }


    public function criteriaMapping(): array
    {

        return [
            'documentNumber' => 'documentNumber.numDocument',
            'email' => 'email.emailAddress',
            'phone' => 'phone.phoneNumber',
        ];

    }
}