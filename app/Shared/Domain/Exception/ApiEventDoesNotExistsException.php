<?php
declare(strict_types = 1);

namespace Shared\Domain\Exception;
use RuntimeException;

final class ApiEventDoesNotExistsException extends RuntimeException
{

}