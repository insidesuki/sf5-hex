<?php

declare(strict_types=1);

namespace Shared\Domain\Exception;

use Exception;

class InvalidDataException extends Exception
{

}