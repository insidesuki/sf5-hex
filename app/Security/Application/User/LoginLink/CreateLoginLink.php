<?php
declare(strict_types=1);

namespace Security\Application\User\LoginLink;

use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;

final class CreateLoginLink
{

    public function __construct(
        private readonly LoginLinkHandlerInterface $loginLinkHandler,
        private readonly UserRepository            $userRepository
    )
    {
    }

    public function handle(string $idUser): LoginLinkDetails
    {
        $user = $this->userRepository->findByIdUser($idUser);
        $loginLinkDetails = $this->loginLinkHandler->createLoginLink($user);
        $loginLinkDetails->getUrl();

        return $loginLinkDetails;

    }

}