<?php
declare(strict_types = 1);

namespace Security\Application\User\SendActivation;

use Security\Domain\User\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

final class SendActivation
{


	public function __construct(
		private readonly ResetPasswordHelperInterface $resetPasswordHelper,
		private readonly MailerInterface $mailer,
		private readonly UserRepository $userRepository
	){}

	/**
	 * @throws ResetPasswordExceptionInterface
	 * @throws TransportExceptionInterface
	 */
	public function handle(string $idUser): ResetPasswordToken
	{

		$user = $this->userRepository->findByIdUser($idUser);
		$resetPasswordToken = $this->resetPasswordHelper->generateResetToken($user);

		$templatedEmail = new TemplatedEmail();
		$templatedEmail->from(
			new Address('sys@iprprevencion.es', 'Welcome')
		);
		$templatedEmail->to($user->email());
		$templatedEmail->subject('Your account is almost active');
		$templatedEmail->htmlTemplate('Registration/PasswordChoice.html.twig');
		$templatedEmail->context(
			[
				'resetToken' => $resetPasswordToken,
				'username' => $user->getUserIdentifier(),
				'name' => $user->name()
			]
		);

		$this->mailer->send($templatedEmail);

		return $resetPasswordToken;


	}


}