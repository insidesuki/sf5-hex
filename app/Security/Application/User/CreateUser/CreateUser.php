<?php
declare(strict_types = 1);

namespace Security\Application\User\CreateUser;

use Security\Application\User\CreateUser\Command\CreateUserCommand;
use Security\Domain\Profile\Repository\ProfileRepository;
use Security\Domain\User\Entity\User;
use Security\Domain\User\Exception\UserAlreadyExistsException;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class CreateUser
{

	public function __construct(
		private readonly UserRepository $userRepository,
		private readonly UserPasswordHasherInterface $userPasswordHasher,
		private readonly ProfileRepository $profileRepository

	){}


	public function handle(CreateUserCommand $createUserCommand):User
	{

		$userEmail = $createUserCommand->email();
		// check if user email not exists
		$userByEmail = $this->userRepository->searchByEmail($userEmail);

		if($userByEmail instanceof User) {
			throw UserAlreadyExistsException::byEmail($userEmail);

		}

        $profile = $this->profileRepository->findByProfile($createUserCommand->profile());

		$newUser = User::createNewUser($createUserCommand,$this->userPasswordHasher,$profile);

		return $this->userRepository->store($newUser);


	}

}