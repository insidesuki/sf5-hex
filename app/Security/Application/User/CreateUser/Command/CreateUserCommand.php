<?php
declare(strict_types = 1);

namespace Security\Application\User\CreateUser\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Security\Domain\User\Command\CreateUserCommandInterface;

final class CreateUserCommand extends DtoSerializer implements CreateUserCommandInterface
{

	private readonly string $name;

	private readonly string $username;

	private readonly string $email;

	private readonly string $profile;

    public function __construct(
        string $name,
        string $username,
        string $email,
        string $profile)
    {
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->profile = $profile;
    }


	public function name(): string
	{
		return $this->name;
	}


	public function username(): string
	{
		return $this->username;
	}


	public function email(): string
	{
		return $this->email;
	}




	public function profile(): string
	{
		return $this->profile;
	}


}