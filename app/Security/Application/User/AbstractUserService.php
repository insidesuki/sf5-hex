<?php
declare(strict_types=1);

namespace Security\Application\User;

use Security\Domain\User\Entity\User;
use Security\Domain\User\Repository\UserRepository;

abstract class AbstractUserService
{

	protected User $user;

	public function __construct(
		protected readonly UserRepository $userRepository
	){}

	protected function getUser(string $idUser):User{

		$this->user = $this->userRepository->findByIdUser($idUser);
		return $this->user;
	}

}