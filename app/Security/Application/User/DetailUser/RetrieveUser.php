<?php
declare(strict_types=1);

namespace Security\Application\User\DetailUser;

use Security\Domain\User\Repository\UserRepository;

final class RetrieveUser
{

	public function __construct(
		private readonly UserRepository $userRepository
	)
	{
	}

	public function handle(string $idUser, UserPresenter $userPresenter): UserPresenter
	{

		$userPresenter->write(
			$this->userRepository->findByIdUser($idUser)
		);
		return $userPresenter;

	}


}