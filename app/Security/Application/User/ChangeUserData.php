<?php
declare(strict_types=1);

namespace Security\Application\User;

use Security\Domain\User\Command\ChangeUserDataCommandInterface;
use Security\Domain\User\Exception\UserAlreadyExistsException;

class ChangeUserData extends AbstractUserService
{

	public function handle(string $idUser, ChangeUserDataCommandInterface $changeUserDataCommand):void{


		$this->getUser($idUser);
		$this->checkEmailDisponibility($changeUserDataCommand->email());


		$this->userRepository->store(
			$this->user
			->changeUserData($changeUserDataCommand)
		);

	}

	private function checkEmailDisponibility(string $email):void
	{

		$userByEmail = $this->userRepository->searchByEmail($email);

		if($userByEmail !== $this->user){
			throw UserAlreadyExistsException::byEmail($email);
		}

	}

}