<?php
declare(strict_types=1);

namespace Security\Application\User\ListUser;

use Security\Domain\User\Repository\UserRepository;

final class ListUsers
{

	public function __construct(
		private readonly UserRepository $userRepository
	)
	{
	}

	public function handle(UsersPresenter $usersPresenter): UsersPresenter
	{

		$usersPresenter->write(
			$this->userRepository->findAll()
		);

		return $usersPresenter;

	}

}