<?php
declare(strict_types = 1);

namespace Security\Application\Recovery;

use Security\Domain\User\Entity\User;
use Security\Domain\User\Repository\UserRepository;
use Security\Infrastructure\Storage\Doctrine\Repository\ResetPasswordRequestRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

final class CreateTokenRecoveryPassword
{

	private ?User $user;

	public function __construct(
		private readonly ResetPasswordHelperInterface $resetPasswordHelper,
		private readonly UserRepository $userRepository,
        private readonly ResetPasswordRequestRepository $resetPasswordRequestRepository,
		private readonly MailerInterface $mailer
	){}

	/**
	 * @throws ResetPasswordExceptionInterface
	 */
	public function handle(string $userEmail): false|ResetPasswordToken
	{


		$this->user = $this->userRepository->searchByEmail($userEmail);


		// Do not reveal whether a user account was found or not.
		if($this->user instanceof User) {

            // remove existed password request for user
            $this->resetPasswordRequestRepository->removeUserPasswordRequest($this->user);

			// create reset token
			$resetToken = $this->resetPasswordHelper->generateResetToken($this->user);

			$this->sendRecoveryEmail($resetToken);
			return $resetToken;

		}

		return false;

	}

	private function sendRecoveryEmail(ResetPasswordToken $resetPasswordToken):void
	{

		$templatedEmail = new TemplatedEmail();
		$templatedEmail->from(
			new Address('sys@iprprevencion.es', 'AccountRecovery')
		);
		$templatedEmail->to($this->user->email());
		$templatedEmail->subject('Your password reset request');
		$templatedEmail->htmlTemplate('ResetPassword/recovery_email_template.html.twig');
		$templatedEmail->context(
			[
				'resetToken' => $resetPasswordToken
			]
		);

		$this->mailer->send($templatedEmail);


	}

}