<?php
declare(strict_types = 1);

namespace Security\Application\Recovery;

use Security\Domain\User\Entity\User;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class ChangeUserPassword
{

	public function __construct(
		private readonly UserRepository $userRepository,
		private readonly UserPasswordHasherInterface $userPasswordHasher,

	){}


	public function handle(string $idUser, string $newPassword): User
	{

		$user = $this->userRepository->findByIdUser($idUser);
		$user->changePassword($newPassword,$this->userPasswordHasher);

		$this->userRepository->store($user);

		return $user;

	}

}