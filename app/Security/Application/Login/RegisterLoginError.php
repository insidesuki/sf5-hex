<?php
declare(strict_types=1);

namespace Security\Application\Login;

use Security\Domain\Log\Entity\Log;
use Security\Domain\Log\Repository\LogRepository;

class RegisterLoginError
{

    public function __construct(
        private readonly LogRepository $logRepository
    )
    {
    }

    public function handle(string $idUser, string $ipAddress, string $userAgent): Log
    {

        return $this->logRepository->store(
            Log::createLoginError($idUser, $ipAddress, $userAgent)
        );


    }

}