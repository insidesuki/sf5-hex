<?php

namespace Security\Infrastructure\Storage\Doctrine\Repository;

use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Security\Domain\User\Entity\User;
use Security\Domain\User\Exception\UserDoesNotExistsException;
use Security\Domain\User\Repository\UserRepository;

final class UserDoctrineRepository extends AbstractDoctrineRepository implements UserRepository
{

	protected static function entityClass(): string
	{
		return User::class;
	}

	public function findByIdUser(string $idUser): User
	{

		$user = $this->findById($idUser);

		if(null === $user) {
			throw new UserDoesNotExistsException();
		}

		return $user;

	}


	public function searchByEmail(string $email): ?User
	{
		return $this->findOne(['email' => $email]);
	}

    public function searchByUsername(string $username): ?User
    {
        return $this->findOne(['username' => $username]);
    }


    public function store(User $user): User
	{
		$this->saveEntity($user);
		return $user;
	}

	public function findAll(): array
	{
		return $this->objectRepository->findAll();
	}
}