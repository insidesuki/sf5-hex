<?php

namespace Security\Infrastructure\Storage\Doctrine\Repository;

use Doctrine\DBAL\Exception;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Security\Domain\Log\Entity\Log;
use Security\Domain\Log\Enum\LogType;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\User\Entity\User;

final class LogDcmRepository extends AbstractDoctrineRepository implements LogRepository
{

	protected static function entityClass(): string
	{
		return Log::class;
	}

	public function findAllByIdUser(string $idUser,int $limit = 20): array
	{
		return $this->objectRepository->findBy([
			'idUser' => $idUser
		],[
			'logDate' => 'DESC'
		],$limit);
	}

	public function store(Log $log): Log
	{
		$this->saveEntity($log);
        return $log;
	}

    /**
     * @throws Exception
     */
    public function countRecentLoginAttemptsUser(User $user): int
    {


        $query = <<<SQL
				
			SELECT count(*) AS total 
			FROM logs A
			WHERE A.id_user = :idUser
			AND A.log_type = :type
			AND A.log_date BETWEEN NOW() - INTERVAL 1 HOUR AND NOW()
			

		SQL;
        return $this->executeRawQuery($query,[
            'idUser'=> $user->idUser(),
            'type' => LogType::loginError->value
        ])->fetchOne();
    }
}