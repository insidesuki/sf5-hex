<?php

namespace Security\Infrastructure\Web\Action;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

final class EmailVerifier
{
    private readonly VerifyEmailHelperInterface $verifyEmailHelper;

    private readonly MailerInterface $mailer;

    private readonly EntityManagerInterface $entityManager;

    public function __construct(VerifyEmailHelperInterface $verifyEmailHelper, MailerInterface $mailer, EntityManagerInterface $entityManager)
    {
        $this->verifyEmailHelper = $verifyEmailHelper;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
    }

    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface $user, TemplatedEmail $templatedEmail): void
    {
        $verifyEmailSignatureComponents = $this->verifyEmailHelper->generateSignature(
            $verifyEmailRouteName,
            $user->idUser(),
            $user->email()
        );

        $context = $templatedEmail->getContext();
        $context['signedUrl'] = $verifyEmailSignatureComponents->getSignedUrl();
        $context['expiresAtMessageKey'] = $verifyEmailSignatureComponents->getExpirationMessageKey();
        $context['expiresAtMessageData'] = $verifyEmailSignatureComponents->getExpirationMessageData();

        $templatedEmail->context($context);

        $this->mailer->send($templatedEmail);
    }


	public function sendEmailRegistered(string $plainPassword, UserInterface $user, TemplatedEmail $templatedEmail): void
	{


		$context = $templatedEmail->getContext();
		$context['plainPassword'] = $plainPassword;
		$context['username'] = $user->username();
		$context['name'] = $user->name();



		$templatedEmail->context($context);

		$this->mailer->send($templatedEmail);
	}



    /**
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, UserInterface $user): void
    {
        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->idUser(), $user->email());

        $user->setIsVerified(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
