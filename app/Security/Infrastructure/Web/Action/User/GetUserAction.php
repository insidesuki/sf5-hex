<?php

namespace Security\Infrastructure\Web\Action\User;

use Security\Domain\User\Repository\UserRepository;
use Shared\Infrastructure\Api\AbstractApiAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class GetUserAction extends AbstractApiAction
{

	public function __construct(
		ValidatorInterface $validator,
		private readonly UserRepository $userRepository
	)
	{
	}

	public function __invoke(string $idUser): JsonResponse
	{

		$user = $this->userRepository->findByIdUser($idUser);

        return $this->responseData(
			[
				'username' => $user->getUserIdentifier(),
				'email' => $user->email(),
				'name' => $user->name(),
				'profile' => $user->profile()->profile
			]
		);


	}

}