<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Web\Action\Reset;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

final class CheckEmailAction extends AbstractController
{
	use ResetPasswordControllerTrait;

	public function __construct(private readonly ResetPasswordHelperInterface $resetPasswordHelper){}

	public function __invoke(): Response
	{

		if(!($resetToken = $this->getTokenObjectFromSession()) instanceof ResetPasswordToken) {
			$resetToken = $this->resetPasswordHelper->generateFakeResetToken();
		}

		return $this->render(
			'ResetPassword/recovery_email_confirmation.html.twig', [
				'resetToken' => $resetToken
			]
		);


	}

}