<?php
declare(strict_types=1);

namespace Security\Infrastructure\Runtime;

use Symfony\Component\Security\Core\User\UserInterface;

final class Security
{
    private readonly ?UserInterface $user;

    public function __construct(private readonly \Symfony\Component\Security\Core\Security $security)
    {
        $this->user = $this->security->getUser();

    }

    public function user(): ?\Symfony\Component\Security\Core\User\UserInterface
    {
        return $this->user;
    }


}