<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Subscriber;

use Security\Application\Login\ManageBadLogin;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use SymfonyCasts\Bundle\ResetPassword\Exception\TooManyPasswordRequestsException;

final class LoginFailSubscriber implements EventSubscriberInterface
{

	public function __construct(private readonly ManageBadLogin $manageBadLogin){}

	/**
	 * @inheritDoc
	 */
	public static function getSubscribedEvents():array
	{
		return [
			LoginFailureEvent::class => 'onLoginFail'
		];
	}

	public function onLoginFail(LoginFailureEvent $loginFailureEvent): void
	{

		try{

			$username = $loginFailureEvent->getRequest()->get('username');

			// exlude oper api username
			if(null === $username){
				return;
			}

			$userAgent = $loginFailureEvent->getRequest()->server->get('HTTP_USER_AGENT') ?? '';
			$remoteIp = $loginFailureEvent->getRequest()->server->get('REMOTE_ADDR');

			if($this->manageBadLogin->handle($username,$remoteIp,$userAgent)){

				$loginFailureEvent->setResponse(new RedirectResponse('blocked'));

			}
		}
		catch(TooManyPasswordRequestsException){
			$loginFailureEvent->setResponse(new RedirectResponse('blocked'));
		}



	}
}