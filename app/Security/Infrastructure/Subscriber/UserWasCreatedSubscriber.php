<?php
declare(strict_types=1);

namespace Security\Infrastructure\Subscriber;

use Security\Application\User\SendActivation\SendActivation;
use Security\Domain\User\Event\UserWasCreated;
use Shared\Application\Service\Person\CreateUserPerson;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;

final class UserWasCreatedSubscriber implements EventSubscriberInterface
{

	public function __construct(
		private readonly SendActivation   $sendActivation,
		private readonly CreateUserPerson $createUserPerson
	)
	{
	}

	public static function getSubscribedEvents(): array
	{
		return [
			UserWasCreated::NAME => [
				[
					'createPersonUser', 0
				],
				[
					'sendActivationLink', 1
				]

			]
		];
	}

	public function createPersonUser(UserWasCreated $userWasCreated):void
	{
		$this->createUserPerson->handle($userWasCreated->idUser);
	}

	/**
	 * @throws ResetPasswordExceptionInterface
	 * @throws TransportExceptionInterface
	 */
	public function sendActivationLink(UserWasCreated $userWasCreated): void
	{

		$this->sendActivation->handle($userWasCreated->idUser);
	}

}