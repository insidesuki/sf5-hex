<?php

namespace Security\Domain\Log\Enum;

use Shared\Domain\Model\Enum\EnumToArray;

enum LogType:string
{

	use EnumToArray;

    case loginSuccess = 'loginSuccess';

    case loginError = 'loginError';


	public function getClassStyle(): string
	{

		return match ($this) {
			self::loginSuccess => 'bg-success-600',
			self::loginError => 'bg-danger-600',
		};

	}



}
