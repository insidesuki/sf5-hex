<?php

namespace Security\Domain\Log\Repository;

use Security\Domain\Log\Entity\Log;
use Security\Domain\User\Entity\User;

interface LogRepository
{

	public function findAllByIdUser(string $idUser,int $limit = 20): array;

	public function store(Log $log): Log;

    public function countRecentLoginAttemptsUser(User $user):int;

}