<?php

namespace Security\Domain\Log\Entity;

use DateTimeImmutable;
use Security\Domain\Log\Enum\LogType;
use Symfony\Component\Uid\Uuid;

class Log
{

	private readonly string            $idLog;

	private readonly string            $idUser;

	private readonly DateTimeImmutable $logDate;

	private readonly string            $remoteAddress;

	private readonly string            $userAgent;

    private readonly LogType $logType;

    private readonly ?string $details;


    private function __construct(
        string  $idUser,
        string  $remoteAddress,
        string  $userAgent,
        LogType  $logType,
        ?string $details
    )
	{
		$this->idUser        = $idUser;
		$this->idLog    = Uuid::v4()->toRfc4122();
		$this->logDate     = new DateTimeImmutable();
		$this->remoteAddress = $remoteAddress;
		$this->userAgent     = $userAgent;
        $this->logType = $logType;
        $this->details = $details;
	}


    public static function createLoginSuccessfull(string $idUser, string $remoteAddress, string $userAgent
    ): Log
	{

		return new self($idUser, $remoteAddress, $userAgent,LogType::loginSuccess,null);
	}

    public static function createLoginError(string $idUser, string $remoteAddress, string $userAgent):Log{

        return new self($idUser,$remoteAddress,$userAgent,LogType::loginError,null);

    }


	public function idLog(): string
	{
		return $this->idLog;
	}

	public function idUser(): string
	{
		return $this->idUser;
	}


	public function logDate(): DateTimeImmutable
	{
		return $this->logDate;
	}

	public function remoteAddress(): string
	{
		return $this->remoteAddress;
	}

	public function userAgent(): string
	{
		return $this->userAgent;
	}

    public function type(): LogType
    {
        return $this->logType;
    }

    public function details(): ?string
    {
        return $this->details;
    }


	public function logDateString():string{

		return $this->logDate->format('d-m-Y H:i');
	}


}