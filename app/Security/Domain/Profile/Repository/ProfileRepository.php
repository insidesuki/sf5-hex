<?php

namespace Security\Domain\Profile\Repository;

use Security\Domain\Profile\Entity\Profile;

interface ProfileRepository
{


	public function findAll():array;

	public function searchByProfile(string $profileName): ?Profile;

	public function findByProfile(string $profileName): Profile;

	public function store(Profile $profile): Profile;

}