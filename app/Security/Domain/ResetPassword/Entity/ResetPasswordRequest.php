<?php

namespace Security\Domain\ResetPassword\Entity;

use DateTimeInterface;
use Symfony\Component\Uid\Uuid;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestTrait;

class ResetPasswordRequest implements ResetPasswordRequestInterface
{
    use ResetPasswordRequestTrait;

    private readonly string $id;

    private readonly object $user;

    public function __construct(object $user, DateTimeInterface $expiresAt, string $selector, string $hashedToken)
    {
		$this->id = Uuid::v4()->toRfc4122();
        $this->user   = $user;
        $this->initialize($expiresAt, $selector, $hashedToken);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUser(): object
    {
        return $this->user;
    }
}
