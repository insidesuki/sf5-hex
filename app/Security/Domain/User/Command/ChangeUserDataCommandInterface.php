<?php

namespace Security\Domain\User\Command;

interface ChangeUserDataCommandInterface
{

	public function name(): string;

	public function email(): string;



}