<?php

namespace Security\Domain\User\Entity;

use DateTime;
use DateTimeImmutable;
use Insidesuki\DDDUtils\Domain\Event\DomainEventPublisher;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\User\Command\ChangeUserDataCommandInterface;
use Security\Domain\User\Command\CreateUserCommandInterface;
use Security\Domain\User\Event\UserWasCreated;
use Shared\Domain\Model\Person;
use Shared\Domain\Model\ValueObject\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{

	private const EXCLUDED_PROFILES_SECURITY = [
		'api'
	];

	private const PASSWORD_EXPIRATION_DAYS = 90;

	private const MAX_LOGIN_ATTEMPTS = 5;

	private readonly string $idUser;

	private readonly string $username;

	private string $name;

	private string $password;

	private string $email;

	private bool $active = false;

	private array $roles = [];

	private readonly Profile $profile;

	private DateTimeImmutable $lastPasswordVerification;

	private bool $isVerified = false;

	private Person $person;


	private function __construct(

		string  $name,
		string  $username,
		string  $email,
		string  $role,
		Profile $profile
	)
	{
		$this->idUser = Uuid::v4()->toRfc4122();
		$this->name = $name;
		$this->username = $username;
		$this->email = $email;
		$this->roles[] = $role;
		$this->profile = $profile;
		$this->lastPasswordVerification = new DateTimeImmutable();
		$this->raiseUserCreatedEvent();


	}

	private function raiseUserCreatedEvent(): void
	{
		DomainEventPublisher::instance()->raise(
			new UserWasCreated(
				$this->idUser, $this->email
			)
		);
	}

	/**
	 * @return static
	 */
	public static function createNewUser(
		CreateUserCommandInterface  $createUserCommand,
		UserPasswordHasherInterface $userPasswordHasher,
		Profile                     $profile
	): self
	{

		$emailAddress = Email::create($createUserCommand->email());

		$newUser = new self(
			$createUserCommand->name(),
			$createUserCommand->username(),
			$emailAddress->emailAddress,
			'ROLE_' . strtoupper($profile->profile),
			$profile
		);
		// hash password
		$newUser->password = $userPasswordHasher->hashPassword(
			$newUser,
			uniqid('Ux86789_dsh' . random_int(0, 9999), true)
		);

		return $newUser;

	}


	public function assignPerson(Person $person):self{

		$this->person = $person;
		return $this;
	}

	public function email(): ?string
	{
		return $this->email;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function person(): Person
	{
		return $this->person;
	}


	public function changeUserData(ChangeUserDataCommandInterface $changeUserDataCommand): self
	{

		$emailAddress = Email::create($changeUserDataCommand->email());
		$this->email = $emailAddress->emailAddress;
		$this->name = $changeUserDataCommand->name();

		return $this;

	}

	public function mustBeRenewed(): bool
	{

		if (in_array($this->profile->profile, self::EXCLUDED_PROFILES_SECURITY)) {
			return false;
		}

		// if is zero, nothing happens
		if (self::PASSWORD_EXPIRATION_DAYS === 0) {
			return true;
		}

		$dateTime = new DateTime();
		$passwordDaysExpiration = date_diff($this->lastPasswordVerification, $dateTime)->format('%a');

		return $passwordDaysExpiration >= self::PASSWORD_EXPIRATION_DAYS;


	}

	public function activate(): void
	{

		$this->active = true;
		$this->isVerified = true;
	}

	public function mustBeBlocked(LogRepository $logRepository): bool
	{

		if (in_array($this->profile->profile, self::EXCLUDED_PROFILES_SECURITY)) {
			return false;
		}


		// if is zero, nothing happens
		if (self::MAX_LOGIN_ATTEMPTS === 0) {
			return false;
		}

		$loginAttempt = $logRepository->countRecentLoginAttemptsUser($this);


		if ($loginAttempt > self::MAX_LOGIN_ATTEMPTS) {

			$this->desactivate();
			return true;

		}

		return false;


	}



	public function desactivate(): void
	{

		$this->active = false;

	}

	public function __toArray(): array
	{

		return [
			'idUser' => $this->idUser,
			'name' => $this->person()->fullName(),
			'personEmail' => $this->person()->email()->emailAddress,
			'personPhone' => $this->person()->phone()->phoneNumber(),
			'identification' => $this->person()->identification()->numDocument(),
			'username' => $this->username,
			'profile' => $this->profile->profile,
			'email' => $this->email,
			'active' => $this->active,
			'complementaryInfo' => $this->person()->otherInfo()
		];


	}

	public function lastPasswordVerification(): DateTimeImmutable
	{
		return $this->lastPasswordVerification;
	}

	public function changePassword(string $plainPassword, UserPasswordHasherInterface $userPasswordHasher): void
	{

		$this->password = $userPasswordHasher->hashPassword(
			$this,
			$plainPassword
		);
		$this->lastPasswordVerification = new DateTimeImmutable();
		$this->active = true;

	}

	public function idUser(): string
	{
		return $this->idUser;
	}

	/**
	 * A visual identifier that represents this user.
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string
	{
		return $this->username;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		//// guarantee every user at least has ROLE_USER
		//$roles[] = 'ROLE_USER';

		return array_unique($roles);
	}

	/**
	 * @see PasswordAuthenticatedUserInterface
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;
		return $this;
	}

	/**
	 * @see UserInterface
	 */
	public function eraseCredentials(): void
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

	public function isActive(): bool
	{
		return $this->active;
	}

	public function isVerified(): bool
	{
		return $this->isVerified;
	}

	public function profile(): Profile
	{
		return $this->profile;
	}

	public function getSalt(): ?string
	{
		return '';
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @deprecated since Symfony 5.3, use getUserIdentifier instead
	 */
	public function username(): string
	{
		return $this->username;
	}
}
