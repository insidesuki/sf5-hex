<?php
declare(strict_types = 1);

namespace Security\Domain\User\Exception;

use RuntimeException;

final class UserDoesNotExistsException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('User does not exists!!');
	}

}