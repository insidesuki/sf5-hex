<?php
declare(strict_types = 1);

namespace Security\Domain\User\Exception;

use RuntimeException;

final class UserAlreadyExistsException extends RuntimeException
{

	private function __construct($message)
	{

		parent::__construct(sprintf('User with %s, already exists!!', $message));

	}

	public static function byEmail(string $email): self
	{

		throw new self($email);

	}

}