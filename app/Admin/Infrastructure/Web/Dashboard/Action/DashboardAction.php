<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\Dashboard\Action;

use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class DashboardAction extends AbstractCommonAction
{

    public function __invoke(Request $request): Response
    {


        return $this->render('dashboard_admin.html.twig');


    }

}