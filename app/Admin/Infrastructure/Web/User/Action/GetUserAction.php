<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\User\Action;

use Admin\Infrastructure\Web\User\Presentation\UserArrayPresenter;
use Security\Application\User\DetailUser\RetrieveUser;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\User\Exception\UserDoesNotExistsException;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Shared\Infrastructure\Web\User\ModifyUser\ModifyUserAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class GetUserAction extends AbstractCommonAction
{


	public function __construct(
		private readonly RetrieveUser     $retrieveUser,
		private readonly LogRepository    $logRepository,
		private readonly ModifyUserAction $modifyUserAction
	)
	{
	}

	public function __invoke(string $idUser, Request $request): Response
	{


		try {

			if ($request->isMethod('POST')) {

				return ($this->modifyUserAction)($idUser, $request);

			}

			$userPresentation = $this->retrieveUser->handle($idUser, new UserArrayPresenter());
			return $this->render('user_detail.html.twig', [
				'user' => $userPresentation->read(),
				'lastLogs' => $this->logRepository->findAllByIdUser($idUser)
			]);


		} catch (UserDoesNotExistsException) {

			return $this->show404();
		} catch (Throwable $throwable) {
			return $this->showErrorPage($throwable);
		}


	}


}