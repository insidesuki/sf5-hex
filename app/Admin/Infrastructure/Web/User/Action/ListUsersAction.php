<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\User\Action;

use Admin\Infrastructure\Web\User\Presentation\ListUsersArrayPresentation;
use Security\Application\User\CreateUser\Command\CreateUserCommand;
use Security\Application\User\CreateUser\CreateUser;
use Security\Application\User\ListUser\ListUsers;
use Security\Domain\Profile\Repository\ProfileRepository;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class ListUsersAction extends AbstractCommonAction
{

	public function __construct(
		private readonly ListUsers $listUsers,
		private readonly ProfileRepository $profileRepository,
		private readonly CreateUser $createUser
	)
	{
	}

	public function __invoke(Request $request): Response
	{

		try {

			// create new user
			if($request->isMethod('POST')){

				$this->createUser->handle(
					CreateUserCommand::createFromArray($request->request->all())
				);

				return $this->redirectToRoute('admin_users');

			}


			$userPresentation = $this->listUsers->handle(new ListUsersArrayPresentation());
			return $this->render('users_list.html.twig', [
					'users' => $userPresentation->read(),
					'profiles' => $this->profileRepository->findAll()
				]
			);


		} catch (Throwable $throwable) {

			return $this->showErrorPage($throwable);
		}


	}


}