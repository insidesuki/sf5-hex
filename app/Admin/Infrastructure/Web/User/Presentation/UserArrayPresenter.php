<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\User\Presentation;

use Security\Application\User\DetailUser\UserPresenter;
use Security\Domain\User\Entity\User;
use Shared\Infrastructure\Presenter\AbstractArrayPresenter;

final class UserArrayPresenter extends AbstractArrayPresenter implements UserPresenter
{

	public function write(User $user): void
	{
		$this->data = $user->__toArray();

	}
}