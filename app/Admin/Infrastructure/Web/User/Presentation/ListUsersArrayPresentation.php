<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\User\Presentation;

use Security\Application\User\ListUser\UsersPresenter;
use Security\Domain\User\Entity\User;
use Shared\Infrastructure\Presenter\AbstractArrayPresenter;

final class ListUsersArrayPresentation extends AbstractArrayPresenter implements UsersPresenter
{

	public function write(array $userCollection): void
	{
		/**
		 * @var User $user
		 */
		foreach ($userCollection AS $user){
			$this->data[] = $user->__toArray();
		}

	}
}