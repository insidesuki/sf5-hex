**InsideSuki-SF6-ProjectSkeleton 6.4**
==============================

Hexagonal architecture for SF-6.4 and PHP > 8.1.
.
The kernel lives in /src, isolated from your application code which lives in the /app folder.

**Features**
==

- Authentification system (dicoupled)
- Restore password  (dicoupled)
- API
- Monolog integration
- PlaniAdmin Template integration

**Installation**
==============================

- composer install
- create a .env.local stored in (config/env folder), se tup your db connection, and mailer DSN
- run migrations.
- 

**Routes**
===========
- /login
- /api/docs


