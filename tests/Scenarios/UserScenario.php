<?php

namespace App\Tests\Scenarios;

use App\Tests\Scenarios\Factories\Security\ProfileFactory;
use Security\Application\User\CreateUser\Command\CreateUserCommand;
use Security\Domain\User\Command\CreateUserCommandInterface;
use Security\Domain\User\Entity\User;
use Shared\Domain\Model\Person;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

trait UserScenario
{

	use PersonScenario;

	public CreateUserCommand $commandCreateUser;

	public function getUser():User{

		$this->setCommandCreateUser();

		$profile = ProfileFactory::create();

		$user = User::createNewUser(
			$this->commandCreateUser,
			$this->createMock(UserPasswordHasherInterface::class),
			$profile

		);

		Person::createFromUser($user);


		return $user;


	}


	public function setCommandCreateUser():void{

		$this->commandCreateUser = new CreateUserCommand(
			'genericName', 'genericUsername',
			'email@generic.es',  'genericprofile'
		);


	}

}