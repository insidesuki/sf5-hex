<?php

namespace App\Tests\Scenarios;

use App\Tests\Scenarios\Factories\Security\UserFactory;
use Security\Domain\User\Entity\User;
use Shared\Domain\Model\Person;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

trait PersonScenario
{


	private User $user;

	public function getPersonUser(): Person
	{
		$this->createUser();;
		return Person::createFromUser($this->user);
	}

	public function createUser(): void
	{
		$passworHasher = $this->createMock(UserPasswordHasherInterface::class);
		$this->user = UserFactory::create($passworHasher);


	}


}