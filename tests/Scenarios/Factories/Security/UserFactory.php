<?php
declare(strict_types=1);

namespace App\Tests\Scenarios\Factories\Security;

use Security\Application\User\CreateUser\Command\CreateUserCommand;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\User\Command\CreateUserCommandInterface;
use Security\Domain\User\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFactory
{

    public static CreateUserCommandInterface $command;

    public static function create(
        UserPasswordHasherInterface   $userPasswordHasher,
        ?Profile $profile = null,
        ?CreateUserCommandInterface $registerUserCommand = null
    ): User
    {

        self::$command = $registerUserCommand ?? new CreateUserCommand(
            'genericName', 'genericUsername',
            'email@generic.es',  'genericprofile'
        );

        $profileUser = $profile ?? ProfileFactory::create();

        return User::createNewUser(
            self::$command,
            $userPasswordHasher,
           $profileUser

        );

    }


}