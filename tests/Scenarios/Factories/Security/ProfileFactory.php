<?php
declare(strict_types=1);

namespace App\Tests\Scenarios\Factories\Security;

use Security\Application\Profile\CreateProfile\Command\CreateProfileCommand;
use Security\Domain\Profile\Command\CreateProfileCommandInterface;
use Security\Domain\Profile\Entity\Profile;

class ProfileFactory
{

    public static CreateProfileCommandInterface $command;

    public static function create(?CreateProfileCommandInterface $createProfileCommand = null): Profile
    {

        self::$command = $createProfileCommand ??   new CreateProfileCommand(
            'genericProfile','genericRoute','genericeTemplate'
        );

        return Profile::create(self::$command);


    }



}