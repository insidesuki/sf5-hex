<?php
declare(strict_types=1);

namespace App\Tests\Scenarios\Factories\Security;

use PHPUnit\Framework\TestCase;
use Security\Application\User\CreateUser\Command\CreateUserCommand;
use Security\Domain\User\Entity\User;
use Security\Infrastructure\Storage\Doctrine\Repository\ProfileDoctrineRepository;
use Security\Infrastructure\Storage\Doctrine\Repository\UserDoctrineRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class PersistedUserFactory extends TestCase
{


    public static function create($container): User
    {

        $profile = $container->get(ProfileDoctrineRepository::class)
            ->store(ProfileFactory::create());

        return $container->get(UserDoctrineRepository::class)->store(
            UserFactory::create(
                (new PersistedUserFactory)->createMock(
                    UserPasswordHasher::class
                ),
                $profile,
                new CreateUserCommand(
                    'nameStored',
                    'usernameStored',
                    'email@stored.es',
                    'role',
                   'profilename'
                )
            )
        );
    }

}