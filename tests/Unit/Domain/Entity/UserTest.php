<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Entity;

use App\Tests\Scenarios\Factories\Security\UserFactory;
use App\Tests\Scenarios\UserScenario;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\User\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;


class UserTest extends TestCase
{

    private UserPasswordHasher|MockObject $passwordHasher;
    private User $user;
	use UserScenario;

    public function setUp(): void
    {
        $this->passwordHasher = $this->createMock(UserPasswordHasher::class);
        $this->user = $this->getUser();
    }


    public function testUserWasCreated(): void
    {

        $user = $this->user;
        $command = $this->commandCreateUser;

		$role = 'ROLE_'.strtoupper($command->profile());

        $this->assertSame($command->email(), $user->email());
        $this->assertSame($command->name(), $user->name());
        $this->assertSame($command->username(), $user->getUserIdentifier());
        $this->assertSame($command->username(), $user->username());
        $this->assertSame($role, $user->getRoles()[0]);
        $this->assertFalse($user->isActive());


    }


    public function testOkChangePassword(): void
    {

        $this->user->changePassword('none', $this->passwordHasher);
        $this->assertTrue($this->user->isActive());
        $this->assertSame($this->user->lastPasswordVerification()->format('d-m-Y'), (new DateTimeImmutable())->format('d-m-Y'));

    }



    public function testReturnTrueOnMustBeBlocked():void{


        $logMockRepo = $this->createMock(LogRepository::class);
        $logMockRepo->method('countRecentLoginAttemptsUser')->willReturn(10);

        $result = $this->user->mustBeBlocked($logMockRepo);

        $this->assertTrue($result);
    }

    public function testReturnFalseOnMustBeBlocked():void{


        $logMockRepo = $this->createMock(LogRepository::class);
        $logMockRepo->method('countRecentLoginAttemptsUser')->willReturn(2);

        $result = $this->user->mustBeBlocked($logMockRepo);

        $this->assertFalse($result);
    }



	public function testArrayWasReturned():void{

		$userArray = $this->user->__toArray();

		$this->assertArrayHasKey('idUser',$userArray);
		$this->assertArrayHasKey('name',$userArray);
		$this->assertArrayHasKey('username',$userArray);
		$this->assertArrayHasKey('profile',$userArray);
		$this->assertArrayHasKey('email',$userArray);
		$this->assertArrayHasKey('active',$userArray);



	}







}
