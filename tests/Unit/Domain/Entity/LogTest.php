<?php

namespace App\Tests\Unit\Domain\Entity;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Security\Domain\Log\Entity\Log;
use Security\Domain\Log\Enum\LogType;

class LogTest extends TestCase
{


	public function setUp(): void
	{
		$this->idUser = '123213213kjkljkl';
		$this->ip = '127.0.0.1';
		$this->agent = 'firefox';
	}

	public function testOkLoginSuccessWasCreated():void
	{

		$login = Log::createLoginSuccessfull($this->idUser,$this->ip,$this->agent);
		$this->assertInstanceOf(Log::class, $login);
		$this->assertSame($this->idUser,$login->idUser());
		$this->assertSame($this->ip,$login->remoteAddress());
		$this->assertSame($this->agent,$login->userAgent());
        $this->assertSame($login->type(),LogType::loginSuccess);
		$this->assertInstanceOf(DateTimeImmutable::class, $login->logDate());

	}

    public function testOkLoginErrorWasCreated():void
    {

        $login = Log::createLoginError($this->idUser,$this->ip,$this->agent);
        $this->assertInstanceOf(Log::class, $login);
        $this->assertSame($this->idUser,$login->idUser());
        $this->assertSame($this->ip,$login->remoteAddress());
        $this->assertSame($this->agent,$login->userAgent());
        $this->assertSame($login->type(),LogType::loginError);
        $this->assertInstanceOf(DateTimeImmutable::class, $login->logDate());

    }

    public function testOkLoginErrorWasCreatedWithoutIdUser():void
    {

        $login = Log::createLoginError('',$this->ip,$this->agent);
        $this->assertEmpty($login->idUser());

    }


}
