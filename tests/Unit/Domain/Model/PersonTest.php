<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Model;

use App\Tests\Scenarios\PersonScenario;
use Insidesuki\ValueObject\String\Email\EmailValue;
use Shared\Domain\Model\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{

	use PersonScenario;


	public function testOkPersonWasCreated():void{

		$person = $this->getPersonUser();
		$this->assertSame($this->user->name(),$person->fullName());
		$this->assertTrue($person->email()->equals(EmailValue::create($this->user->email())));
		$this->assertSame($this->user,$person->user());
		$this->assertSame($this->user->person(),$person);

	}

}
