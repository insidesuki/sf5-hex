<?php

declare(strict_types=1);

namespace App\Tests\Unit\Application\UseCase\Profile;

use PHPUnit\Framework\TestCase;
use Security\Application\Profile\CreateProfile\Command\CreateProfileCommand;
use Security\Application\Profile\CreateProfile\CreateProfile;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\Profile\Repository\ProfileRepository;

/**
 * @group unit
 */
class CreateProfileTest extends TestCase
{
    public function testOkWhenProfileDoesExists(): void
    {

        $repository = $this->createMock(ProfileRepository::class);
        $createProfile = new CreateProfile($repository);
        $command = new CreateProfileCommand(
            'testProfile',
            '/default',
            'baseHtml'
        );
        $profile = $createProfile->handle($command);

        $this->assertSame($profile->profile, $command->profile());
        $this->assertSame($profile->defaultRoute(), $command->defaultRoute());

    }

    public function testOkWhenProfileAlreadyExists(): void
    {

        $existedProfile = Profile::create(
            new CreateProfileCommand(
                'testProfile',
                '/default/test',
                'baseHtml'
            )
        );

        $repository = $this->createMock(ProfileRepository::class);
        $repository->expects($this->any())
            ->method('searchByProfile')
            ->willReturn($existedProfile);

        $createProfile = new CreateProfile($repository);
        $command = new CreateProfileCommand(
            'testProfile',
            '/default',
            'baseHtml'
        );
        $profile = $createProfile->handle($command);

        $this->assertSame($profile->profile, $command->profile());
        $this->assertSame($profile->defaultRoute(), $existedProfile->defaultRoute());

    }



}
