<?php
declare(strict_types=1);

namespace App\Tests\Unit\Application\UseCase\Login;

use PHPUnit\Framework\TestCase;
use Security\Application\Login\RegisterLoginError;
use Security\Domain\Log\Entity\Log;
use Security\Domain\Log\Enum\LogType;
use Security\Domain\Log\Repository\LogRepository;

class RegisterLoginErrorTest extends TestCase
{

    public function testErrorLoginWasCreated():void{

        $mockRepo = $this->createMock(LogRepository::class);
        $registerLoginError = new RegisterLoginError($mockRepo);

        $idUser = 'idUser';
        $ip = '127.0.0.1';
        $userAgent = 'Firefox';

        $mockRepo->method('store')->willReturn(
            Log::createLoginError($idUser,$ip,$userAgent)
        );

        $logError = $registerLoginError->handle($idUser,$ip,$userAgent);

        $this->assertSame($idUser,$logError->idUser());
        $this->assertSame($ip,$logError->remoteAddress());
        $this->assertSame($userAgent,$logError->userAgent());
        $this->assertSame(LogType::loginError,$logError->type());


    }

}
