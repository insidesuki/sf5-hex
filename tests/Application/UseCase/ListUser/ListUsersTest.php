<?php
declare(strict_types=1);

namespace App\Tests\Application\UseCase\ListUser;

use Admin\Infrastructure\Web\User\Presentation\ListUsersArrayPresentation;
use App\Tests\Scenarios\Factories\Security\UserFactory;
use App\Tests\Scenarios\UserScenario;
use PHPUnit\Framework\TestCase;
use Security\Application\User\ListUser\ListUsers;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class ListUsersTest extends TestCase
{



	use UserScenario;


	public function testOkListUsers():void{

		$userMockRepo = $this->createMock(UserRepository::class);
		$user1 = $this->getUser();
		$user2 = $this->getUser();
		$expectedResult = [$user1,$user2];
		$userMockRepo->method('findAll')->willReturn($expectedResult);
		$listUsers = new ListUsers($userMockRepo);
		$presentation = $listUsers->handle(new ListUsersArrayPresentation());
		$this->assertCount(count($expectedResult),$presentation->read());





	}

}
