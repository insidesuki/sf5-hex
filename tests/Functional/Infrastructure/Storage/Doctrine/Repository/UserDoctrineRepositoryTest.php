<?php
declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Storage\Doctrine\Repository;

use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractIntegrationTest;
use Security\Domain\User\Repository\UserRepository;
use Security\Infrastructure\Storage\Doctrine\Repository\UserDoctrineRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class UserDoctrineRepositoryTest extends AbstractIntegrationTest
{

	private UserRepository $userRepository;

	public function setUp(): void
	{
		parent::setUp();;
		$this->userRepository = $this->getService(UserDoctrineRepository::class);
	}


	public function testListAllUser():void{


		$userCollection = $this->userRepository->findAll();
		$this->assertIsArray($userCollection);
		$firstUser = $userCollection[0];
		$this->assertInstanceOf(UserInterface::class,$firstUser);



	}



}
