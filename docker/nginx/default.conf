server {
    listen 80;
    server_name localhost;
    root /application/public;
    proxy_buffer_size   128k;
    proxy_buffers   4 256k;
    proxy_busy_buffers_size   256k;

    location / {
        try_files $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /index.php/$1 last;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass docker-app-skeleton:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;

        # increase buffers to solve symfony exceptions issues
        # @url https://stackoverflow.com/questions/17708152/nginx-overwrites-general-symfony-errors-with-502-bad-gateway
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;

    }

    error_log /var/log/nginx/app_sym54_hex_error.log;
    access_log /var/log/nginx/app_sym54_hex_access.log;
}